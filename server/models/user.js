import mongoose from 'mongoose';
const { Schema } = mongoose;

const UserSchema = new Schema({
  fullname: {
    type: String,
    required: [true, 'Isi fullname']
  },
  username: {
    type: String,
    required: [true, 'Isi username cuy']
  },
  password: {
    type: String
  },
  role: String,
  status: {
    type: String,
    enum: ['Solo', 'Duet', 'Triplet', 'Quartet']
  }
});

UserSchema.virtual('date')
  .get(() => this._id.getTimestamp());


module.exports = mongoose.model('User', UserSchema);
